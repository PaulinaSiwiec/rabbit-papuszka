package com.example.demo;

import org.springframework.stereotype.Component;

@Component
public class PapuszkaListener {

    public void halo(PapuszkaZatelefonowalaEvent halo) {
        System.out.println(halo.getImiePapiuszki() + " tururururu");
    }
}
