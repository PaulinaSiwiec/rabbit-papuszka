package com.example.demo;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitOperations;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@SpringBootApplication
public class DemoApplication {

	@Bean
	MessageConverter getConverter() {
		return new Jackson2JsonMessageConverter();
	}

	@Bean
	MessageListenerAdapter adapter (PapuszkaListener listener){
		MessageListenerAdapter adapter = new MessageListenerAdapter(listener, "halo");
		adapter.setMessageConverter(getConverter());
		return adapter;
	}

	@Bean
	SimpleMessageListenerContainer container(ConnectionFactory factory, MessageListenerAdapter adapter) {
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(factory);
		container.setQueueNames("kolej");
		container.setMessageListener(adapter);

		return container;
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);


	}
}



class PapuszkaZatelefonowalaEvent {
	@JsonProperty
	private String imiePapiuszki;
	@JsonProperty
	private String rodzajPapuszki;

	public PapuszkaZatelefonowalaEvent(String imiePapiuszki, String rodzajPapuszki) {
		this.imiePapiuszki = imiePapiuszki;
		this.rodzajPapuszki = rodzajPapuszki;
	}

	public PapuszkaZatelefonowalaEvent() {

	}

	public String getImiePapiuszki() {
		return imiePapiuszki;
	}

	public void setImiePapiuszki(String imiePapiuszki) {
		this.imiePapiuszki = imiePapiuszki;
	}

	public String getRodzajPapuszki() {
		return rodzajPapuszki;
	}

	public void setRodzajPapuszki(String rodzajPapuszki) {
		this.rodzajPapuszki = rodzajPapuszki;
	}
}

@Component
class Start implements CommandLineRunner {
	@Autowired
	RabbitOperations rabbit;

	@Override
	public void run(String... args) throws Exception {
		rabbit.convertAndSend("dupa", "d", new PapuszkaZatelefonowalaEvent("mimi", "misiak"));
	}

}